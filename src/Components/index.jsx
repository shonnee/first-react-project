import React from "react";
import styled from "styled-components";

import { IconApp } from './aside/Icon';
import { BodyApp } from './aside/Body';

const Container = styled.div`

    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    margin: 0px;
    padding: 0px;
    box-sizing: border-box;
    padding: 5px;

`;

export function IndexApp(params) {

    return (

            <Container>

                <IconApp 
                    miniature={params.Icon} 
                    bkColorIcon={params.bkColorIcon}
                />
                
                <BodyApp 
                    text={params.text} 
                    description={params.description} 
                    link={params.link}
                />

            </Container>

    );
}

