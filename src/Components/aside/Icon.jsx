import React from "react";
import styled from "styled-components";

const Container = styled.div`

    width: 38%;
    height: 100%;

`;

const Body = styled.div`

    box-sizing: border-box;
    background-color: ${props => props.colorIcon};
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 25px;

`;

const Icon = styled.img`

    width: 100%;
    height: 80%;

`

export function IconApp({miniature, bkColorIcon}) {
    return (

        <Container>

            <Body colorIcon={bkColorIcon}>

                <Icon src={miniature} />

            </Body>

        </Container>

    );
}

