import React from "react";
import styled from "styled-components";

const Container = styled.div`

    width: 100%;
    height: 100%;
    padding-left: 15px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;

`;

const Title = styled.p`

    color: #5c6170;
    font-weight: bold;
    font-family: verdana;
    font-size: 15px;
    margin: 0px;
    display: flex;
    justify-content: space-between;
    align-items: center;

`;

const Desactivated = styled.span`

    width: 32%;
    height: 100%;
    background-color: #efeef4;
    font-size: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #7b828e;


`

const Description = styled.p`

    color: #9c9ea4;
    font-family: verdana;
    font-size: 12px;
    margin: 5px 0px;

`;

const Enlace = styled.a`

    font-family: verdana;
    font-size: 14px;
    color: #5591ac;


`

export function BodyApp({text, description, link}) {

    return (

        <Container>

            <Title>
                {`${text}`}
                <Desactivated>
                    Desactivado
                </Desactivated>
            </Title>
            <Description>
                {`${description}`}
            </Description>
            <Enlace>
                {`${link}`}
            </Enlace>

        </Container>

    );
}

