import React from 'react'
import styled from 'styled-components'

const Bar = styled.ul`

    box-sizing: border-box;
    width: 100%;
    margin: 0px;
    padding: 0px;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    list-style: none;

`

const Option = styled.li`

    box-sizing: border-box;
    display: flex;
    color: #5c6170;
    font-size: 17px;
    font-family: verdana;
    fit-content: contain;
    text-decoration: none;
    margin: 20px 10px;
    :first-child {
        text-decoration: underline 2px #5591ac;
        text-underline-offset: 20px;
    }

`

export const NavBarApp = ({txt1, txt2, txt3}) => {
    
    return (
    
        <Bar>
            <Option>
                {`${txt1}`}
            </Option>
            <Option>
                {`${txt2}`}
            </Option>
            <Option>
                {`${txt3}`}
            </Option>
        </Bar>

    )

}
