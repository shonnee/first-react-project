import React from "react";
import styled from "styled-components";
import { IndexApp } from './components';

import crown from './img/crown.png';
import browser from './img/browser.png';
import envelope from './img/envelope.png';
import { NavBarApp } from "./components/header/NavBar";
import { DataSectionApp } from "./components/section/DataSection";

const BigContainer = styled.div`
    
    width: 100%;
    box-sizing: border-box;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    padding: 20px;

`;

const Container = styled.div`

    width: 38%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-end;

`;

function App() {
  
  return (

        <BigContainer>

            <NavBarApp 
                txt1={'Resumen'}
                txt2={'Administración'}
                txt3={'Configuración de tienda online'}
            />

            <DataSectionApp
                listMsg1={'Pedidos'}
                listMsg2={'Analítica de negocio'}
                listMsg3={'Clientes'}
                listMsg4={'Descuentos'}
                listMsg5={'Formularios recibidos'}
            />

            <Container>

                <IndexApp 
                    Icon={crown} 
                    bkColorIcon={"#fff6e2"}
                    text={"Paquete Premium"}
                    description={"Descubre nuevas funciones"}
                    link={"Hazte Premium"}
                />
                <IndexApp 
                    Icon={browser} 
                    bkColorIcon={"#ecfcf1"}
                    text={"Dominio"}
                    description={"Registra tu propio dominio, p. ej. www.mipaginaweb.com"}
                    link={"Registrar un nuevo dominio"}
                />
                <IndexApp 
                    Icon={envelope} 
                    bkColorIcon={"#f3f2fe"} 
                    text={"Cuentas de email"}
                    description={"Envía correos profesionales"}
                    link={"Crear nueva cuenta de email"}
                />

            </Container>

        </BigContainer>

  );
}

export default App;
