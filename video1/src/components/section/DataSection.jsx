import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight, faCartShopping, faHandshake, faChartLine, faPercent, faRectangleList } from '@fortawesome/free-solid-svg-icons'

const DataDiv = styled.section`

    box-sizing: border-box;
    width: 60%;
    height: 200px;
    margin-top: 20px;
    padding: 0px;
    display: flex;
    justify-content: space-between;

`;

const SectionContainer = styled.div`

    width: 100%;
    height: 140%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    box-shadow: 0px 0px 5px 1px whitesmoke;

`;

const IconAndListContainer = styled.div`

    border: 0.05px solid whitesmoke;
    border-bottom: none;
    width: 100%;
    display: flex;
    padding: 5px;
    :last-child { border-bottom: 0.5px solid whitesmoke; };
    :nth-child(3) { background-color: whitesmoke; };

`;

const IconDiv = styled.span`

    box-sizing: border-box;
    width: 10%;
    height: 100%;
    font-size: 20px;
    color: #92c1d0;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 5px;

`;

const ListContainer = styled.div`

    box-sizing: border-box;
    width: 90%;
    height: 100%;
    font-family: verdana;
    font-size: 12px;
    font-weight: bold;
    color: #5c6170;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 5px;
    
`;

const SpanDiv = styled.span`

    width: 3.5%;
    text-align: center;
    margin: 4px;
    font-size: 12px;
    border-radius: 50px;
    color: white;
    background-color: red;
    
`;

const MiniSpan = styled.span`

    width: 17%;

`;

export const DataSectionApp = ({listMsg1, listMsg2, listMsg3, listMsg4, listMsg5}) => {
    
    return (

        <DataDiv>
            <SectionContainer>
                <IconAndListContainer>
                        <IconDiv>
                            <FontAwesomeIcon icon={ faCartShopping } />
                        </IconDiv>
                    <ListContainer>
                        <MiniSpan>
                        {`${listMsg1}`}
                            <SpanDiv>
                                <SpanDiv>
                                    3
                                </SpanDiv>
                            </SpanDiv>
                        </MiniSpan>
                            <IconDiv>
                                <FontAwesomeIcon icon={ faAngleRight } />
                            </IconDiv>
                    </ListContainer>
                </IconAndListContainer>
                <IconAndListContainer>
                        <IconDiv>
                            <FontAwesomeIcon icon={ faChartLine } />
                        </IconDiv>
                    <ListContainer>
                        {`${listMsg2}`}
                            <IconDiv>
                                <FontAwesomeIcon icon={ faAngleRight } />
                            </IconDiv>
                    </ListContainer>
                </IconAndListContainer>
                <IconAndListContainer>
                        <IconDiv>
                            <FontAwesomeIcon icon={ faHandshake } />
                        </IconDiv>
                    <ListContainer>
                        {`${listMsg3}`}
                            <IconDiv>
                                <FontAwesomeIcon icon={ faAngleRight } />
                            </IconDiv>
                    </ListContainer>
                </IconAndListContainer>
                <IconAndListContainer>
                        <IconDiv>
                            <FontAwesomeIcon icon={ faPercent } />
                        </IconDiv>
                    <ListContainer>
                        {`${listMsg4}`}
                            <IconDiv>
                                <FontAwesomeIcon icon={ faAngleRight } />
                            </IconDiv>
                    </ListContainer>
                </IconAndListContainer>
                <IconAndListContainer>
                        <IconDiv>
                            <FontAwesomeIcon icon={ faRectangleList } />
                        </IconDiv>
                    <ListContainer>
                        {`${listMsg5}`}
                            <IconDiv>
                                <FontAwesomeIcon icon={ faAngleRight } />
                            </IconDiv>
                    </ListContainer>
                </IconAndListContainer>
            </SectionContainer>
        </DataDiv>

    )

}
